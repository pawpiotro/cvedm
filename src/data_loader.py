import os
import re


def natural_key(pair):
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', pair[0])]


def load_train_file(train_file_path):
    data_for_classification = []
    no_data = 0
    with open(train_file_path, 'r') as file:
        for line in file:
            tmp_line = line.split(";")
            if len(tmp_line) < 2:
                no_data += 1
            else:
                tmp_desc = tmp_line[0].split()
                data_for_classification.append((tmp_desc, tmp_line[-1]))
    return data_for_classification, no_data


def load_train_files(train_files_paths):
    data_for_classification = []
    no_data = 0
    for file_path in train_files_paths:
        print(file_path)
        if os.path.isfile(file_path):
            print(" File: " + str(file_path))
            temp_data, temp_no_data = load_train_file(file_path)
            print(" " + str(len(temp_data)) + " labeled entries read.")
            print(" " + str(temp_no_data) + " had no target set.")
            data_for_classification.extend(temp_data)
            no_data += temp_no_data
        # elif os.path.isdir(file_path):
        #     files = os.listdir(file_path)
        #     for file in files:
        #         path = os.path.join(file_path, file)
        #         # print(" File: " + str(path))
        #         # temp_data, temp_no_data = load_train_file(path)
        #         # print(" " + str(len(temp_data)) + " labeled entries read.")
        #         # print(" " + str(temp_no_data) + " had no target set.")
        #         # data_for_classification.extend(temp_data)
        #         # no_data += temp_no_data

    # sort data by CVE id
    cve_ids = []
    for i in range(len(data_for_classification)):
        cve_ids.append(data_for_classification[i][0][0])

    data_for_classification = [x for (y, x)
                               in sorted(zip(cve_ids, data_for_classification), key=natural_key)]

    # for i in range(len(data_for_classification)):
    #     print(data_for_classification[i][0][0])

    return data_for_classification, no_data


def load_classify_file(file_to_classify_path):
    data_for_classification = []
    no_data = 0
    with open(file_to_classify_path, 'r') as file:
        for line in file:
            tmp_line = line.split(";")
            tmp_desc = tmp_line[0].split()
            if len(tmp_line) < 2:
                data_for_classification.append((tmp_desc, ''))
                no_data += 1
            else:
                data_for_classification.append((tmp_desc, tmp_line[-1]))
    return data_for_classification, no_data


def process_data(entries, data_for_classification, mode):
    res_data = []
    res_target = []
    entries_index = 0
    for elem in data_for_classification:
        while entries[entries_index].e_name != elem[0][0] and entries_index < (len(entries) - 1):
            entries_index += 1

        e_cpe_data = ''
        for i in range(2, len(elem[0])):
            tokens = elem[0][i].split('_')
            for j in range(len(tokens)):
                if len(tokens[j]) > 2:
                    e_cpe_data += tokens[j] + ' '

        if mode == 0:
            res_data.append(e_cpe_data + entries[entries_index].e_desc)
        elif mode == 1:
            res_data.append(e_cpe_data)
        elif mode == 2:
            res_data.append(entries[entries_index].e_desc)

        res_target.append(elem[1].strip())
    return res_data, res_target

