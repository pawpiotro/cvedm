from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.pipeline import Pipeline
from nltk.corpus import stopwords


class Vectorizer:

    def __init__(self):
        self.vect = CountVectorizer()
        self.tfidf = TfidfTransformer()

        vect_parameters = {
            'analyzer': 'word',
            # 'tokenizer': LemmaTokenizer(),
            'max_features': 2000,
            # 'stop_words': lemma_stopwords,
            'stop_words': stopwords.words('english'),
            'max_df': 0.7,
            'min_df': 0.01,
            'ngram_range': (1, 1)
        }

        tfidf_parameters = {
            # 'use_idf': (True, False),
            # 'norm': ('l1', 'l2')
        }

        self.vect.set_params(**vect_parameters)
        self.tfidf.set_params(**tfidf_parameters)

    def fit(self, data):
        self.vect.fit(data)

    def print_vocabulary(self):
        print(self.vect.vocabulary_)

    def transform(self, data):
        v_data = self.vect.transform(data)
        self.tfidf.fit(v_data)
        return self.tfidf.transform(v_data)
