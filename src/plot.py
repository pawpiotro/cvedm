import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
import seaborn as sns


class Plot:

    @staticmethod
    def plot_confusion_matrix(y_true, y_pred, title,
                              normalize=False,
                              cmap=plt.cm.Blues):
        classes = ['A', 'C', 'E', 'H', 'M', 'P', 'S']
        cm = confusion_matrix(y_true, y_pred, classes)
        # classes = unique_labels(y_true, y_pred)
        # print("Classes: " + str(classes))

        if normalize:
            cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
            title += " - Normalized confusion matrix"
            # title += " - (normalizacja)"
        else:
            title += " - Confusion matrix, without normalization"
            # title += " - (brak normalizacji)"

        print(title)
        print(cm)

        fig, ax = plt.subplots()
        fmt = '.2f' if normalize else 'd'
        sns.heatmap(cm, cmap=cmap, annot=True, ax=ax, fmt=fmt, square=True, cbar=True)

        ax.set_xlabel('Predicted labels')
        ax.set_ylabel('True labels')
        # ax.set_xlabel('Predykcje klasyfikatora')
        # ax.set_ylabel('Poprawne klasy')
        ax.set_title(title)
        ax.xaxis.set_ticklabels(classes)
        ax.yaxis.set_ticklabels(classes)

        plt.margins(1, 1)
        fig.tight_layout()

    @staticmethod
    def save_fig(fname):
        plt.savefig(fname)

    @staticmethod
    def show_plot():
        plt.show()

        # works with matplotlib 3.1.1 - temporary error with displaying figure

        # if normalize:
        #     cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        #     title = 'Normalized confusion matrix'
        # else:
        #     title = 'Confusion matrix, without normalization'
        #
        # print(title)
        # print(cm)
        #
        # fig, ax = plt.subplots()
        # im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
        # ax.figure.colorbar(im, ax=ax)
        # ax.set(xticks=np.arange(cm.shape[1]),
        #        yticks=np.arange(cm.shape[0]),
        #        xticklabels=classes, yticklabels=classes,
        #        title=title,
        #        ylabel='True label',
        #        xlabel='Predicted label',
        #        aspect='equal')
        #
        # plt.margins(1, 1)
        #
        # fmt = '.2f' if normalize else 'd'
        # thresh = cm.max() / 2.
        # for i in range(cm.shape[0]):
        #     for j in range(cm.shape[1]):
        #         ax.text(j, i, format(cm[i, j], fmt),
        #                 ha="center", va="center",
        #                 color="white" if cm[i, j] > thresh else "black")
        # fig.tight_layout()
        # plt.show()
