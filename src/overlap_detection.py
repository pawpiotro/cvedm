from sklearn.cluster import KMeans
from sklearn.pipeline import Pipeline
from sklearn.metrics.cluster import homogeneity_score, adjusted_rand_score
import scipy.spatial.distance as dist
from scipy import mean


class OverlappingDectector:

    def __init__(self):
        self.classes_dict = {}
        self.means = {}
        self.overlapping_entries = {
            "A": 0,
            "C": 0,
            "E": 0,
            "H": 0,
            "M": 0,
            "P": 0,
            "S": 0
        }

    def measure_distance(self, a, b):
        return dist.cosine(a, b)

    def check_overlapping(self, data, targets):
        for index in range(0, len(data)):
            target = targets[index]
            if target in self.classes_dict:
                self.classes_dict[target].append(data[index])
            else:
                self.classes_dict[target] = [data[index]]

        for cl in self.classes_dict:
            print(cl)
            tmp = self.classes_dict.get(cl)
            print(len(tmp))
            self.means[cl] = mean(tmp, axis=0)
            # print(mean(tmp, axis=0))
            # print(len(self.classes_dict))
            # print(self.classes_dict.keys())
            # print(self.classes_dict.get('H'))

        for cl in self.classes_dict:
            tmp = self.classes_dict.get(cl)
            others = [x for x in self.classes_dict if x is not cl]
            # print(others)
            for entry in tmp:
                cl_mean_dist = self.measure_distance(self.means[cl], entry)
                for other in others:
                    if self.measure_distance(self.means[other], entry) < cl_mean_dist:
                        self.overlapping_entries[cl] = self.overlapping_entries[cl] + 1
                        break

        print(self.overlapping_entries)
    # report = {}
    #
    # def __init__(self):
    #     self.pipeline = Pipeline([
    #         ('kmeans', KMeans())
    #     ])
    #
    #     parameters = {
    #         'kmeans__n_clusters': 7
    #     }
    #
    #     self.pipeline.set_params(**parameters)
    #
    # def predict(self, data):
    #     return self.pipeline.fit_predict(data)
    #
    # def print_stats(self, true, predicted):
    #     print("Homogeneity_score: " + str(homogeneity_score(true, predicted)))
    #     print("Adjusted_rand_score: " + str(adjusted_rand_score(true, predicted)))

