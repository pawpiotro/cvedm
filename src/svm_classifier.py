from sklearn import svm
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.pipeline import Pipeline
from nltk.corpus import stopwords
from sklearn.metrics import classification_report, accuracy_score
from src.plot import Plot
import pandas as pd


class Classifier:

    report = {}

    def __init__(self):
        self.pipeline = Pipeline([
            ('clf', svm.SVC())
        ])

        parameters = {
            'clf__kernel': 'rbf',
            'clf__gamma': 0.8,
            'clf__C': 20,
            'clf__decision_function_shape': 'ovr',
            'clf__cache_size': 1000,
            'clf__max_iter': -1
        }

        self.pipeline.set_params(**parameters)

    def train_model(self, data, target):
        self.pipeline.fit(data, target)

    def predict(self, data):
        return self.pipeline.predict(data)

    def print_stats(self, true, predicted, normalized, title):
        # target_names = ['Chips & chipsets', 'Enterprise', 'Home', 'Mobile devices', 'PC/laptops', 'SCADA/Industrial']
        plot = Plot()
        print(classification_report(true, predicted))  # , target_names=target_names))
        self.report = classification_report(true, predicted, output_dict=True)
        print("\n")
        print("Accuracy score:" + str(accuracy_score(true, predicted)))
        print("\n")
        if normalized:
            plot.plot_confusion_matrix(true, predicted, title, True)
        else:
            plot.plot_confusion_matrix(true, predicted, title)

    def classification_report_csv(self, path):
        if not self.report:
            return
        df = pd.DataFrame(self.report).transpose()
        df.to_csv(path)
