class Entry(object):
    __slots__ = ('e_name', 'e_type', 'e_desc')

    def __init__(self, e_name, e_type, e_desc):
        self.e_name = e_name
        self.e_type = e_type
        self.e_desc = e_desc

    def __str__(self):
        return self.e_name + " " + self.e_type + " " + self.e_desc
