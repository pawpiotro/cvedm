from lxml import etree
from src.cve_entry import Entry

NSMAP = {
    'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    'xmlns': 'http://cve.mitre.org/cve/downloads/1.0',
    'xsi:noNamespaceSchemaLocation': 'https://cve.mitre.org/schema/cve/cve_1.0.xsd'
}


def find_in_tree(tree, node):
    found = tree.find(node)
    if found is None:
        print("No %s in file" % node)
        found = []
    return found


def parse_xml(xml_file):
    res_entries = []
    tree = etree.parse(xml_file)
    root = tree.getroot()

    items = root.xpath('//xmlns:cve/xmlns:item', namespaces=NSMAP)
    for item in items:
        item_name = item.attrib['name']
        item_type = item.attrib['type']
        item_desc = item.xpath('./xmlns:desc/text()', namespaces=NSMAP)[0]
        res_entries.append(Entry(item_name, item_type, item_desc))
        # print(item.attrib['name'])
        # print(item.attrib['type'])
        # print(item.xpath('./xmlns:desc/text()', namespaces=NSMAP)[0])
    return res_entries
