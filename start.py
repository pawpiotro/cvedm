import classifier
import ntpath
import datetime
import sys
import os
import pathlib

files = ['./data/dict2010-5-annotated-1.txt',
         './data/dict2011-5-annotated-1.txt',
         './data/dict2012-5-annotated-1.txt',
         './data/dict2013-5-annotated-1.txt',
         './data/dict2014-5-annotated-1.txt',
         './data/dict2015-5-annotated-1.txt',
         './data/dict2016-5-annotated-1.txt',
         './data/dict2017-5-annotated-2.txt',
         './data/dict2018-5-annotated-1.txt',
         './data/dict2019-5-annotated-1.txt']

timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')
user_path = timestamp
logs_path = "./output/" + user_path + "/logs/"

if not os.path.exists(logs_path):
    pathlib.Path(logs_path).mkdir(parents=True, exist_ok=True)


argv_flags = ['-m', "-f", '-p', user_path]

for i in range(0, len(files)):
    training_files = [files[i]]
    files_names = "_" + str(ntpath.basename(files[i]).split(".")[0].split("-")[0][-2:])
    for j in range(i+1, len(files)):
        # print("T:", training_files)
        # print("C:", files[j])
        # print("TN:", files_names)
        # print("CN:", ntpath.basename(files[j]).split(".")[0].split("-")[0][-2:])
        log_name = "C_" + str(ntpath.basename(files[j]).split(".")[0].split("-")[0][-2:]) + "_T" + files_names
        # print(log_name)

        # Redirect stdout to save logs to file
        log_file = logs_path + log_name + ".txt"
        sys.stdout = open(log_file, 'w+')
        # Classify

        # Set arguments
        argv = []

        # Add training files paths
        for tfile in training_files:
            argv.append("-t")
            argv.append(tfile)

        # Add file to classify path
        argv.append("-c")
        argv.append(files[j])

        # Add flags
        argv.extend(argv_flags)
        # print(argv)
        classifier.main(argv)
        training_files.append(files[j])
        files_names += "_" + str(ntpath.basename(files[j]).split(".")[0].split("-")[0][-2:])
