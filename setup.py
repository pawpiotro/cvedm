import requests

# download CVE database XML
URL = "https://cve.mitre.org/data/downloads/allitems.xml"
print("Downloading CVE database...")
response = requests.get(URL)
with open('./allitems.xml', 'wb') as file:
    file.write(response.content)
print("CVE database downloaded successfully.")
