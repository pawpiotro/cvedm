import os
import pathlib
import sys
import re
import getopt
import io
import datetime
import ntpath
from sklearn.model_selection import train_test_split
from src.xml_parser import parse_xml
from src.svm_classifier import Classifier
from src.overlap_detection import OverlappingDectector
from src.vectorizer import Vectorizer
import src.data_loader as dl
from src.plot import Plot
import pandas as pd

import warnings
warnings.filterwarnings('ignore')


def main(argv):
    main__res_path = "./output/"
    db_file_path = "./allitems.xml"
    user_path = ""
    train_files_paths = []
    file_to_classify_path = ''
    test_size = 0.2
    split_data = False
    metrics = False
    rnd = False
    desc = False
    output = False
    normalized_metrics = False
    figs_to_file = False
    quality_check = False
    mode = 0

    if len(argv) == 0:
        print("python classifier.py [-h] for help.")
        sys.exit(2)

    try:
        opts, args = getopt.getopt(argv, "a:ht:c:s:dfmnop:qr", ["mode=", "training_file=", "classify_file=", "split_ratio="])
    except getopt.GetoptError:
        print("python classifier.py -h  --  for help.")
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print("python classifier.py [-dfhmnps] [-t training_file] [-c classify_file] [-s split_ratio]\n"
                  "-t, --training_file=     -- path to training file (or files given like -t arg1 -t arg2 etc.).\n"
                  "-c, --classify_file=     -- path to file for classification.\n"
                  "-s, --split_ratio=       -- ratio of data split (training, split) for file given in -t path.\n"
                  "-h                       -- help.\n"
                  "-d                       -- adds CVE description to classification output file.\n"
                  "-m                       -- after classifying file given in -c path"
                  " prints metrics for labeled entries.\n"
                  "-n                       -- normalized metrics\n"
                  "-a, --mode=              -- program mode (which data will be used)\n"
                  "                             0 -- using all data (default)\n"
                  "                             1 -- using only CPE data\n"
                  "                             2 -- using only CVE description.\n"
                  "-r                       -- random train/test data split.\n"
                  "-f                       -- save matrices (figs) instead of displaying.\n"
                  "-p                       -- path for custom results folder name.\n"
                  "-q                       -- quality check for training data.\n")
            sys.exit()
        elif opt in ("-t", "--training_file"):
            if not isinstance(arg, (list, tuple)):
                arg = [arg]
            train_files_paths.extend(arg)
        elif opt in ("-c", "--classify_file"):
            file_to_classify_path = arg
        elif opt in ("-a", "--mode"):
            try:
                mode = int(arg)
            except ValueError:
                print("Mode parameter not valid. Should be number 0-2")
                sys.exit()

            if mode < 0 or mode > 2:
                print("Mode parameter not in valid range (0-2)")
                sys.exit()
        elif opt in ("-s", "--split_ratio"):
            try:
                test_size = int(arg) / 100
            except ValueError:
                print("Test size parameter not valid.")
                sys.exit()

            if test_size < 0 or test_size > 1.0:
                print("Test size not in valid range (0-100)")
                sys.exit()
            split_data = True
        elif opt == "-m":
            metrics = True
        elif opt == "-r":
            rnd = True
        elif opt == "-d":
            desc = True
        elif opt == "-o":
            output = True
        elif opt == "-n":
            normalized_metrics = True
        elif opt == "-f":
            figs_to_file = True
        elif opt == "-p":
            user_path = arg
        elif opt == "-q":
            quality_check = True

    if not os.path.exists(db_file_path):
        print("No xml file found. Run setup.py.")
        sys.exit(1)
    for path_file in train_files_paths:
        # print(path_file)
        if not os.path.exists(path_file):
            print("No training file found. Check given paths.")     # ignore invalid files?
            sys.exit(1)

    classifier = Classifier()
    plot = Plot()

    # Organize training files and prepare filenames for saving results.
    train_files_paths = set(train_files_paths)
    files_names = []
    for path_file in train_files_paths:
        files_names.append(ntpath.basename(path_file).split(".")[0].split("-")[0][-2:])
    files_names.sort()
    files_names_string = ""
    for f in files_names:
        files_names_string += "_" + str(f)
    print(files_names_string)
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')

    # Prepare paths for saving results
    if user_path is "":
        user_path = timestamp + "/"
    user_path = user_path.strip()
    user_path = re.sub(r'[\\/\:*"<>\|\.%\$\^&£]', '', user_path)
    if user_path[0] is "/":
        user_path = user_path[1:]
    if user_path[-1] is not "/":
        user_path = user_path + "/"

    classification_res_file_path = main__res_path + user_path + "classified_data/"
    figs_res_file_path = main__res_path + user_path + "figs/"
    report_res_file_path = main__res_path + user_path + "reports/"

    if not os.path.exists(classification_res_file_path):
        pathlib.Path(classification_res_file_path).mkdir(parents=True, exist_ok=True)

    # Loading data
    print("Reading data...")
    start = datetime.datetime.now()
    entries = parse_xml(db_file_path)
    print(str(len(entries)) + " entries read from xml file. Time elapsed - "
          + str(datetime.datetime.now() - start) + ".")

    start = datetime.datetime.now()
    data_for_classification, no_data_entries = dl.load_train_files(train_files_paths)
    print(str(len(data_for_classification)) + " overall labeled entries read from train file. Time elapsed - "
          + str(datetime.datetime.now() - start) + ".")
    print(str(no_data_entries) + " overall had no target set.")
    if len(data_for_classification) < 1:
        print("Not enough labeled data to train model.")
        sys.exit(1)

    data, target = dl.process_data(entries, data_for_classification, mode)

    # Vectorize data
    # tududud vectorizing
    vectorizer = Vectorizer()
    vectorizer.fit(data)
    v_data = vectorizer.transform(data)
    # # df = pd.DataFrame(v_data.toarray())
    # df = v_data.toarray()
    # print(df)

    if quality_check:
        print("jest git")
        detector = OverlappingDectector()
        detector.check_overlapping(v_data.toarray(), target)

    # detector = OverlappingDectector()
    #
    # y_pred = detector.predict(data)
    # detector.print_stats(target, y_pred)

    # # Training classifier
    # print("Fitting classifier...")
    # start = datetime.datetime.now()
    # if split_data:
    #     if rnd:
    #         x_train, x_test, y_train, y_test = train_test_split(v_data, target, test_size=test_size)
    #     else:
    #         x_train, x_test, y_train, y_test = train_test_split(v_data, target, test_size=test_size, random_state=42)
    #     classifier.train_model(x_train, y_train)
    #     print("Model trained. Time elapsed - " + str(datetime.datetime.now() - start) + ".")
    #     print("Testing classifier...")
    #     start = datetime.datetime.now()
    #     y_pred = classifier.predict(x_test)
    #     print("Data classified.  Time elapsed - " + str(datetime.datetime.now() - start) + ".")
    #     if file_to_classify_path is '':
    #         classifier.print_stats(y_test, y_pred, normalized_metrics, "Training data")
    #         res_fig_path = figs_res_file_path + "T" + "_T" + files_names_string + ".png"
    #         plot.save_fig(res_fig_path)
    # else:
    #     classifier.train_model(v_data, target)
    #     y_pred = classifier.predict(v_data)
    #     if file_to_classify_path is '':
    #         classifier.print_stats(target, y_pred, normalized_metrics, "Training data")
    #         if figs_to_file:
    #             res_fig_path = figs_res_file_path + "T" + "_T" + files_names_string + ".png"
    #             plot.save_fig(res_fig_path)
    #
    # # Classifying
    # if file_to_classify_path is not '':
    #
    #     if not os.path.exists(file_to_classify_path):
    #         print("No file for classification found. Check given path.")
    #         sys.exit(1)
    #
    #     print("Classifying given file...")
    #     print("Loading file for classification...")
    #     start = datetime.datetime.now()
    #     data_for_classification, no_data_entries = dl.load_classify_file(file_to_classify_path)
    #     print(str(len(data_for_classification)) + " entries read from dict file. Time elapsed - " +
    #           str(datetime.datetime.now() - start) + ".")
    #     print(str(no_data_entries) + " not labeled entries.")
    #
    #     class_data, class_target = dl.process_data(entries, data_for_classification, mode)
    #     v_class_data = vectorizer.transform(class_data)
    #
    #     print("Classifying data...")
    #     start = datetime.datetime.now()
    #     y_pred = classifier.predict(v_class_data)
    #     print("Data classified.  Time elapsed - " + str(datetime.datetime.now() - start) + ".")
    #
    #     if not os.path.exists(classification_res_file_path):
    #         os.mkdir(classification_res_file_path)
    #     res_file_path = classification_res_file_path + "C_"\
    #                     + str(ntpath.basename(file_to_classify_path).split(".")[0].split("-")[0][-2:]) \
    #                     + "_T" + files_names_string + ".txt"
    #
    #     f = io.open(res_file_path, mode="w+", encoding="utf-8")
    #
    #     if desc:
    #         for i in range(len(y_pred)):
    #             f.write("%s - %s; %s\n" % (data_for_classification[i][0][0], y_pred[i],  class_data[i]))
    #     else:
    #         for i in range(len(y_pred)):
    #             f.write("%s - %s\n" % (data_for_classification[i][0][0], y_pred[i]))
    #     f.close()
    #     print("Results saved to file.")
    #
    #     if output:
    #         for i in range(len(y_pred)):
    #             print("%s   - %s" % (data_for_classification[i][0][0], y_pred[i]))
    #
    #     if metrics:
    #         print("Entries for classification: " + str(len(class_data)))
    #         num_labeled_data = len([e for e in class_target if e is not ''])
    #         print("Labeled entries: " + str(num_labeled_data))
    #         print("Predictions: " + str(len(y_pred)))
    #         if num_labeled_data < 1:
    #             print("Not enough labeled data.")
    #             sys.exit(1)
    #         temp_pred = []
    #         temp_true = []
    #         for i in range(len(class_target)):
    #             if class_target[i] != '':
    #                 temp_pred.append(y_pred[i])
    #                 temp_true.append(class_target[i])
    #
    #         classifier.print_stats(temp_true, temp_pred, normalized_metrics, "Data for classification")
    #         if figs_to_file:
    #             if not os.path.exists(figs_res_file_path):
    #                 pathlib.Path(figs_res_file_path).mkdir(parents=True, exist_ok=True)
    #             if not os.path.exists(report_res_file_path):
    #                 pathlib.Path(report_res_file_path).mkdir(parents=True, exist_ok=True)
    #             file_name = "C_" + str(ntpath.basename(file_to_classify_path).split(".")[0].split("-")[0][-2:]) \
    #                            + "_T" + files_names_string
    #             res_fig_path = figs_res_file_path + file_name + ".png"
    #             plot.save_fig(res_fig_path)
    #
    #             res_report_path = report_res_file_path + file_name + ".csv"
    #             classifier.classification_report_csv(res_report_path)
    #
    # if not figs_to_file:
    #     plot.show_plot()


if __name__ == "__main__":
    main(sys.argv[1:])
