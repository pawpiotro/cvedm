import sys
import time
from lxml import etree
# from collections import Counter
from sklearn.feature_extraction.text import CountVectorizer

import nltk
nltk.download('stopwords')
nltk.download('punkt')

from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import word_tokenize, RegexpTokenizer

import numpy as np


def find_in_tree(tree, node):
    found = tree.find(node)
    if found is None:
        print("No %s in file" % node)
        found = []
    return found


class Entry(object):
    __slots__ = ('name', 'type', 'desc')

    def __init__(self, name, type, desc):
        self.name = name
        self.type = type
        self.desc = desc

    def __str__(self):
        return self.name + " " + self.type + " " + self.desc


NSMAP = {
    'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    'xmlns': 'http://cve.mitre.org/cve/downloads/1.0',
    'xsi:noNamespaceSchemaLocation': 'https://cve.mitre.org/schema/cve/cve_1.0.xsd'
}
xml_file = "./raw_data/allitems.xml"
vw_file = "./vw.cve.txt"


def parse_xml():
    entries = []
    tree = etree.parse(xml_file)
    root = tree.getroot()

    items = root.xpath('//xmlns:cve/xmlns:item', namespaces=NSMAP)
    for item in items:
        iname = item.attrib['name']
        itype = item.attrib['type']
        idesc = item.xpath('./xmlns:desc/text()', namespaces=NSMAP)[0]
        entries.append(Entry(iname, itype, idesc))
        # print(item.attrib['name'])
        # print(item.attrib['type'])
        # print(item.xpath('./xmlns:desc/text()', namespaces=NSMAP)[0])
    return entries


def bag_of_words(words):
    bag = [0]*len(words)
    for w in words:
        for i, word in enumerate(words):
            if word == w:
                bag[i] += 1
    return bag

# convert to bag of words representation
# TODO: do poprawki, dlugo mieli


def convert_to_bow(doc):
    # tokens = word_tokenize(doc)
    tokenizer = RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(doc)
    stop_words = set(stopwords.words('english'))
    tokens = [w for w in tokens if not w in stop_words]
    porter = PorterStemmer()
    stems = []
    for t in tokens:
        stems.append(porter.stem(t))
    bow = bag_of_words(stems)
    result = ''
    for i, s in enumerate(stems):
        result += (s + ":" + str(bow[i]) + " ")
    return result


def build_vw_file(entries):
    with open(vw_file, 'wb') as file:
        for entry in entries:
            file.write(entry.name.encode('ascii', 'ignore') + " " + convert_to_bow(entry.desc.encode('ascii','ignore')) + '\n')
            # file.write(entry.name.encode('ascii', 'ignore') + " " + entry.desc.encode('ascii','ignore') + '\n')


def convert_to_bow2(doc, vectorizer):
    x = vectorizer.fit_transform(doc).toarray()
    result = ''
    stems = vectorizer.get_feature_names()
    # print(stems)
    # print(x)
    for i in range(0,len(stems)):
        result += (stems[i] + ":" + str(x[0][i]) + " ")
    return result


def build_vw_file2(entries):
    # my_tokenizer = RegexpTokenizer(r'\w+')
    stop_words = set(stopwords.words('english'))
    vectorizer = CountVectorizer(lowercase=True, stop_words=stop_words, ngram_range=(1,1))
    with open(vw_file, 'wb') as file:
        for entry in entries:
            file.write(entry.name.encode('ascii', 'ignore') + " " + convert_to_bow2([entry.desc.encode('ascii','ignore')], vectorizer) + '\n')


def main():
    start = time.time()
    print("Parsing XML data file...")
    entries = parse_xml()
    end = time.time()
    print("XML data file succesfully parsed in: " + str(end-start))
    print("Database size: " + str(len(entries)))
    start = time.time()
    print("Building Vowpal Wabbit file...")
    build_vw_file2(entries)
    end = time.time()
    print("Vowpal Wabbit file successfully built in: " + str(end-start))


if __name__ == "__main__":
    main()
