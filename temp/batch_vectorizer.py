import artm

batch_vectorizer = artm.BatchVectorizer(data_path='./vw.cve.txt',
                                        data_format='vowpal_wabbit',
                                        target_folder='my_collection_batches')


dictionary = artm.Dictionary()
dictionary.gather(data_path='my_collection_batches')
dictionary.save_text(dictionary_path='./my_collection_batches/my_dictionary.txt')
