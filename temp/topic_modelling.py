import artm

batch_vectorizer = artm.BatchVectorizer(data_path='./my_collection_batches',
                                        data_format='batches')

my_dictionary = artm.Dictionary()
my_dictionary.load_text(dictionary_path='./my_collection_batches/my_dictionary.txt')

#model = artm.ARTM(num_topics=20, dictionary=my_dictionary)
#model.initialize(dictionary=my_dictionary)

lda = artm.LDA(num_topics=15, alpha=0.01, cache_theta=True, num_document_passes=5, dictionary=my_dictionary)
lda.fit_offline(batch_vectorizer=batch_vectorizer, num_collection_passes=10)

print lda.perplexity_value

top_tokens = lda.get_top_tokens(num_tokens=10)
for i, token_list in enumerate(top_tokens):
    print("Topic " + str(i) + ": " + str(token_list))
